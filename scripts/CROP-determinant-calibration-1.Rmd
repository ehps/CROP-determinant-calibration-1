---
title: "Collaborative Research in an Open Platform: Determinant Calibration 1"
author: "THE CROP authors et al."
date: "`r format(Sys.time(), '%H:%M:%S on %Y-%m-%d %Z (UTC%z)')`"
output:
  html_document:
    toc: true
    code_folding: hide
editor_options: 
  chunk_output_type: console
---

```{r setup, include=FALSE}


###-----------------------------------------------------------------------------
### Packages
###-----------------------------------------------------------------------------

if (!(installed.packages()['ufs', 'Version'] >= "0.4")) {
  stop("You need to have at least version 0.4 of the `ufs` package installed; ",
       "install it with:\n\ninstall.packages('ufs');");
}

ufs::checkPkgs(
  'here',         ### Using paths relative to the project root
  'knitr',        ### Knitting documents and controlling the knitting
  'kableExtra',   ### Pretty tables
  'poorman',      ### For bind.rows()
  'patchwork'     ### For combining plots
);

### Install limonaid from GitLab
ufs::quietGitLabUpdate("r-packages/limonaid");

###-----------------------------------------------------------------------------
### Settings
###-----------------------------------------------------------------------------

knitr::opts_chunk$set(cache=FALSE);
knitr::opts_chunk$set(comment=NA);
knitr::opts_chunk$set(echo = TRUE);

ufs::opts$set(debug = FALSE);

###-----------------------------------------------------------------------------
### Set the variables with the paths and filenames
###-----------------------------------------------------------------------------

dataPath <- here::here('results-data-raw');
processedDataPath <- here::here('results-data-processed');
outputPath <- here::here('results-output');
scriptPath <- here::here("scripts");

###-----------------------------------------------------------------------------
### Set convenience vectors
###-----------------------------------------------------------------------------

behaviors <- c("exerciseRAA", "smokingRAA");
behaviorRegexed <- c("exerciseBehavior", "smokingBehavior");

determinantLabels <-
  c("Attitude", "Perceived\nNorms", "Perceived\nBehavioral\nControl");

countryLabels <-
  c(de = "Germany",
    fr = "France",
    hr = "Croatia",
    ie = "Ireland",
    it = "Italy",
    nl = "The Netherlands");

```

# Determinant structure specification

This command builds the determinant structures that we will analyse. This uses in-progress code from the `behaviorchange` package that leverages the `data.tree` package to build a hierarchy of determinants and sub-determinants.

```{r}

determinantStructures <-
  lapply(
    behaviors,
    function(behav) {
      return(
        behaviorchange::determinantStructure(
          behav,
          list(behaviorRegEx = behav),
          behaviorchange::determinantVar(
            "intention",
            "_in",
            behaviorchange::determinantVar(
              "attitude",
              "_Att"),
            behaviorchange::determinantVar(
              "perceivedNorm",
              "_pn"),
            behaviorchange::determinantVar(
              "perceivedBehavioralControl",
              "_pbc"
            )
          )
        )
      );
    }
  );

```

# Read data

```{r read-data-through-limesurvey-api, eval=FALSE}

###-----------------------------------------------------------------------------
### This section should import the data directly from LimeSurvey. However,
### the server sends an odd response.
###-----------------------------------------------------------------------------

### Load LimeSurvey credentials from a file that isn't synched to Git
source(
  file.path(
    scriptPath,
    "CROP-determinant-calibration-1_PRIVATE_.R"
  )
);

### Set the LimeSurvey credentials in the options
options(lime_api      = ls3_api_url);
options(lime_username = ls3_user_user);
options(lime_password = ls3_user_pass);

### Create a LimeSurvey session key
#ls3_session_key <- limer::get_session_key();

### Contents of that function
  body.json = list(
    method = "get_session_key",
    id = " ",
    params = list(admin = ls3_user_user, password = ls3_user_pass)
  )
  r <- httr::POST(
    ls3_api_url,
    httr::content_type_json(),
    body = jsonlite::toJSON(body.json, auto_unbox = TRUE)
  )
  session_key <- as.character(
    jsonlite::fromJSON(
      httr::content(r,
                    encoding = "utf-8"))$result
    );
  session_cache$session_key <- session_key
  session_key

### Get a list of all survey IDs we have access to
list_surveys <-
  limer::call_limer(method = "list_surveys");
sids <-
  as.character(
    sort(
      as.numeric(
        unique(
          list_surveys$sid
        )
      )
    )
  );

```

```{r read-data-from-disk, warning=FALSE}
### (hiding "incomplete final line" warnings)

###-----------------------------------------------------------------------------
### Read all data files stored in `dataPath`
###-----------------------------------------------------------------------------

### Survey files
dataFiles <-
  list.files(dataPath, pattern="\\.csv")

### Get survey identifiers
surveyIds <-
  gsub(
    ".*---(..)---survey_(\\d{6})_R_.*",
    "\\2",
    dataFiles
  );

### Get countries
surveyCountries <-
  gsub(
    ".*---(..)---survey_(\\d{6})_R_.*",
    "\\1",
    dataFiles
  );

dataList <-
  lapply(
    surveyIds,
    limonaid::ls_import_data,
    path = dataPath,
    categoricalQuestions =
      c(
        "informedConsent",
        "gender",
        "educationPresent",
        "educationPast",
        "exerciseBehavior",
        "smokingBehavior"
      )
  );
names(dataList) <- surveyCountries;

dat <-
  do.call(
    poorman::bind_rows,
    lapply(
      surveyCountries,
      function(country) {
        dataList[[country]]$country <- country;
        return(dataList[[country]]);
      }
    )
  );

```

## Questions {.tabset .tabset-pills}

Unfortunately, the R export plugin for LimeSurvey always exports the labels in the primary language. Therefore, only the English-language question texts (specifically, those for Ireland) are shown here.

<!-- ### Overview -->

<!-- These tabs show the question texts in each country. -->

```{r question-labels, results='asis'}

currentCountry <- "ie";

# for (currentCountry in surveyCountries) {
#   ufs::heading(countryLabels[currentCountry]);
  questionDf <-
    data.frame(
      variable = names(dataList[[currentCountry]]),
      question = c(
        attributes(dataList[[currentCountry]])$label,
        rep(
          "",
          ncol(dataList[[currentCountry]]) -
            length(attributes(dataList[[currentCountry]])$label)
        )
      )
    );
  questionDf$question <-
  ifelse(grepl("^\\[.+\\].*", questionDf$question),
         gsub("^\\[(.+)\\].*", "\\1", questionDf$question),
         questionDf$question);
               
  print(
    kableExtra::kable_styling(
      knitr::kable(
        questionDf,
        row.names = FALSE
      )
    )
  );
# }

```

## Data preprocessing

```{r determinant-structure-preprocessing, results="asis"}

### Add variables names of the determinants' measures and compute
### means where necessary
for (i in seq_along(determinantStructures)) {
  behaviorchange::detStructAddVarNames(
    determinantStructures[[i]],
    names(dat)
  );
  dat <- behaviorchange::detStructComputeScales(
    determinantStructures[[i]],
    dat
  );
}

### Create convenience vectors with the variable names per behavior
scaleVars<- list();
intentionVars <- list();
determinantVars <- list();
for (i in seq_along(behaviors)) {
  scaleVars[[behaviors[i]]] <-
    unname(determinantStructures[[i]]$Get('scaleVarName'));
  scaleVars[[behaviors[i]]] <-
    scaleVars[[behaviors[i]]][!is.na(scaleVars[[behaviors[i]]])];

  ### Create separate variables for intention and the other determinants
  intentionVars[[behaviors[i]]] <-
    grep("intention", scaleVars[[behaviors[i]]], value=TRUE);
  determinantVars[[behaviors[i]]] <-
    setdiff(scaleVars[[behaviors[i]]], intentionVars[[behaviors[i]]]);
}

```

# Analyses

## Sample descriptions {.tabset .tabset-pills}

The total dataset (all countries, after sanitization) has `r nrow(dat)` rows. `r sum(!is.na(dat$submitdate))` rows submitted the complete survey (i.e. have a non-missing value for `submitdate`). Note that providing demographic data was optional, so less data may be available for the demographic variables (age, sex, education, country, and region).

```{r sample-descriptions, results='asis'}

dat$duration_minutes <- dat$Total.time/60;

for (currentCountry in surveyCountries) {
  ufs::heading(countryLabels[currentCountry]);

  print(
    rosetta::descr(
      dat[dat$country == currentCountry, ],
      items = c(
        'age',
        'gender',
        'educationPresent',
        'educationPast',
        'exerciseBehavior',
        'smokingBehavior'
      ),
      histogram = TRUE,
      headingLevel = 4
    )
  );
    
}

```

## CIBERlite plots

```{r}

CIBERlite_plots <- list();

for (i in seq_along(behaviors)) {
  CIBERlite_plots[[behaviors[i]]] <- list();
  for (currentCountry in surveyCountries) {
    CIBERlite_plots[[behaviors[i]]][[currentCountry]] <-
      behaviorchange::CIBERlite(
        data = dat[dat$country == currentCountry, ],
        determinants = determinantVars[[behaviors[i]]],
        targets = intentionVars[[behaviors[i]]],
        determinantLabels = determinantLabels,
        title = countryLabels[currentCountry]
      );
  }
}

wrappedPlots <-
  lapply(
    CIBERlite_plots,
    patchwork::wrap_plots,
    ncol=3
  );

wrappedPlots$exerciseRAA <-
  wrappedPlots$exerciseRAA +
    patchwork::plot_annotation(
      title = "CIBERlite plots for exercise",
      subtitle = "Bars are rescaled sample means, diamonds 95% confidence intervals for correlations"
    );

wrappedPlots$smokingRAA <-
  wrappedPlots$smokingRAA +
    patchwork::plot_annotation(
      title = "CIBERlite plots for smoking",
      subtitle = "Bars are rescaled sample means, diamonds 95% confidence intervals for correlations"
    );

ufs::knitAndSave(
  wrappedPlots$exerciseRAA,
  figCaption = "CIBERlite plots for exercise",
  figWidth = 9,
  figHeight = 6,
  path = outputPath
);

ufs::knitAndSave(
  wrappedPlots$smokingRAA,
  figCaption = "CIBERlite plots for smoking",
  figWidth = 9,
  figHeight = 6,
  path = outputPath
);

```

```{r export-data}

names(dat)[names(dat) == "Group.time..Informations.générales"] <-
  "Group_time__general_info";
names(dat)[names(dat) == "Group.time..General.Information"] <-
  "Group_time__general_info2";

haven::write_sav(
  dat,
  file.path(
    dataPath,
    "CROP-determinant-calibration-1--merged-data.sav"
  )
);

```